(fn map [array fun]
  (icollect [_ v (ipairs array)] (fun v)))

(fn pmap [array fun]
  (icollect [_ v (pairs array)] (fun v)))

(fn pt->hash [array]
  (collect [_ [k v] (ipairs array)] (values k v)))

(fn hash->pt [array]
  (icollect [k v (pairs array)] [k v]))

(fn filter [array fun]
  (icollect [_ v (ipairs array)] (when (fun v) v)))

(fn fold [array init fun]
  (accumulate [acc init key value (ipairs array)]
    (fun acc value)))

(fn in [ar key] (fold ar false #(if (= key $2) true $1)))

(fn ifold [array init fun]
  (accumulate [acc init key value (ipairs array)]
    (fun acc key value)))

(fn split0 [st]
  (fcollect [i 1 (# st)]
    (string.sub st i i)))

(fn split [st key]
  (icollect [s (string.gmatch st (.. "[^" key "]+"))] s))

(fn scollect [st key]
  (let [accu []
        iterator (string.gmatch st key)]
    (var v nil)
    (while (do (set v [(iterator)]) (> (# v) 0))
      (table.insert accu v)
      )
    accu))

(fn unique [array]
  (local u {})
  (each [_ value (ipairs array)]
    (when (not (. u value))
      (tset u value value)))
  (icollect [_ v (pairs u)] v))

(fn merge-t [ar1 ar2]
  (local ar3 [])
  (each [key value (ipairs (or ar1 []))]
    (table.insert ar3 value))
  (each [key value (ipairs (or ar2 []))]
    (table.insert ar3 value))
  ar3)

(fn heaps [ar len fun]
    (if (> len 1)
        (for [i 1 len]
          (heaps ar (- len 1) fun)
          (if (= (% len 2) 1)
              (let [first (. ar 1)
                    last (. ar len)]
                (tset ar 1 last)
                (tset ar len first))
              (let [index (. ar i)
                    last (. ar len)]
                (tset ar i last)
                (tset ar len index))))
        (fun ar)))

{: map
 : filter
 : fold
 : ifold
 : split0
 : split
 : scollect
 : in
 : heaps
 : pt->hash
 : hash->pt
 : unique
 : merge-t}
