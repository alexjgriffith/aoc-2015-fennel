(fn session-token []
  (with-open [f (io.open "session.private")]
    (assert f "Failed to load session.private from root directory.

Either copy your advent of code session key into session.private, or
manually copy the input for the day under source/{day#}.

Example: source/1
")
    (f:read :*all)))

(fn request-url [day year endpoint]
  (if endpoint
      (.. "https://adventofcode.com/" year "/day/" day "/" endpoint)
      (.. "https://adventofcode.com/" year "/day/" day)))

(fn request-input [day year]
  (local http (require :socket.http))
  (local ltn12 (require :ltn12))
  (local source (request-url day year "input"))
  (local st (session-token))
  (local headers {:Cookie (.. :session= st)})
  (local body [])
  (local (_body status-code header status-text)
         (http.request {:method "GET" :url source :headers headers
                        :sink (ltn12.sink.table body)}))
  (assert (and (= 200 status-code) (~= (# body) 0))
          (..
           "Failed to download input from advent of code site
url: " source "
status-code: " status-code "
status-text: " status-text))
  
  (var ret "")
  (each [_ s (ipairs body)]
    (set ret (.. ret s)))
  (if (= (string.sub ret (# ret)) "\n")
      (string.sub ret 1 (- (# ret) 1))
      ret))

(fn file-exists [name]
  (let [f (io.open name :r)
        ret (~= f nil)]
    (when ret (io.close f))
    ret))

(fn download-input-to-source [day year]
  "Download the input for a specific day and year of advent of code.
The file is saved under source/{#day}.
You will have to create a session.private file in the root directory
of this project which contains your advent of code session token."
  (assert (require :socket.http) "Advent of Code requires socket.http

Either install socket.http or manually copy the input for each
advent of code day into source/{day#}

Example source/1
")
  (assert (and (> day 0) (< day 26)) "day must be between 1 and 25.")
  (assert (and (> year 2014)) "Sorry there is no advent of code pre 2015.")
  (let [file (.. :source/ day)
        st (request-input day year)]
    (when (not (file-exists file))
      (with-open [f (io.open file :w)]
        (f:write st)))))

(fn day-to-string [day]
  "Load the string for a specific day of advent of code.
Day should be between 1 and 25.
This function loads text from source/{day#}. Make sure
you populate that file before you call this functions."
  (assert (and (> day 0) (< day 26)) "day must be between 1 and 25.")
  (with-open [f (io.open (.. :source/ day))]
    (assert f (.. "Failed to open " (.. :source/ day) "
Try downloading the input with `download-input-to-source.'"))
    (f:read :*all)))

{: download-input-to-source
 : day-to-string}
