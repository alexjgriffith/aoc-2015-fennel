(local advent-of-code (require :lib.advent-of-code))

(advent-of-code.download-input-to-source 2 2015)

(local input (advent-of-code.day-to-string 2))

(local {: split} (require :split))
(local day "AOC 2015 Day 2")

(local dimension-strings (-> input (split "\n")))

(local dimensions (icollect [_ st (ipairs dimension-strings)]
                    (let [[l w h] (split st "x")]
                      {:l (tonumber l)
                       :w (tonumber w)
                       :h (tonumber h)})))

;; Part I
(local tests [{:l 2 :w 3 :h 4} 58
              {:l 1 :w 1 :h 10} 43])

(fn sa [{: l : w : h}]
  (local dim-a (* l w))
  (local dim-b (* l h))
  (local dim-c (* h w))
  (local min (math.min (math.min dim-a dim-b) dim-c))
  (+ (* 2 dim-a) (* 2 dim-b) (* 2 dim-c) min))

(fn sum-sa [dimensions]
  (var output 0)
  (each [key value (ipairs dimensions)]
    (set output (+ output (sa value))))
  output)

(when false
  (for [i 1 (# tests) 2]
  (let [key (. tests i)
        value (. tests (+ i 1))]
    (print (sum-sa [key]) value)))
  )

(print (.. day "  Part I:  " (sum-sa dimensions)))

;; Part II
(fn ribbon [{: l : w : h}]
  (local p-a (+ l l w w))
  (local p-b (+ l l h h))
  (local p-c (+ h h w w))
  (local min-p (math.min (math.min p-a p-b) p-c))
  (local bow (* l w h))
  (+ bow min-p))

(fn sum-ribbon [dimensions]
  (var output 0)
  (each [key value (ipairs dimensions)]
    (set output (+ output (ribbon value))))
  output)

(sum-ribbon [{:l 2 :w 3 :h 4}])

(sum-ribbon [{:l 1 :w 1 :h 10}])

(print (.. day "  Part II: " (sum-ribbon dimensions)))
