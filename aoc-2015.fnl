(fn file-exists [name]
  (let [f (io.open name :r)
        ret (~= f nil)]
    (when ret (io.close f))
    ret))

(for [i 1 25]
  (let [file (.. "day-" i)
        alt (.. file "-alt")]
    (if
     (file-exists (.. alt ".fnl")) (require alt)
     (file-exists (.. file ".fnl")) (require file))))
