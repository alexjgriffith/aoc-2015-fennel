(local advent-of-code (require :lib.advent-of-code))
(advent-of-code.download-input-to-source 13 2015)
(local input (advent-of-code.day-to-string 13))
(local day "AOC 2015 Day 13")
(local {: map : fold : scollect : heaps : pt->hash : hash->pt : split : unique
        : merge-t}
       (require :lib.utils))

(import-macros {: incr} :macros)

(local relations
       (->
        input
        (scollect "(%w+) would (%w+) (%d+) happiness units by sitting next to (%w+).")
        (map (fn [[who status amount nextto]]
               [(.. who "->" nextto)
                (* (if (= status :gain) 1 -1) (tonumber amount))]))
        (pt->hash)))

(local names
       (-> relations
           hash->pt
           (map #(split (. $ 1) "->"))
           (fold [] #(do (table.insert $1 (. $2 1))
                         (table.insert $1 (. $2 2))
                         $1))
           unique))

(fn score [names]
  (var lname nil)
  (var s 0)
  (each [_ name (ipairs names)]
    (let [key (if lname
                  (.. lname "->" name)
                  (.. (. names (# names)) "->" name))]
      (incr s (or (. relations key) 0)))
    (let [key (if lname
                  (.. name "->" lname)
                  (.. name "->" (. names (# names))))]
      (incr s (or (. relations key) 0)))
    (set lname name))
  s)

(do
  (var p 0)
  (fn c [order] (let [s (score order)] (when (> s p) (set p s))))
  (heaps names (# names) c)
  (print (.. day " Part I:  " p)))

(do
  (var p 0)
  (fn c [order] (let [s (score order)] (when (> s p) (set p s))))
  (local names2 (merge-t names [:Me]))
  (heaps names2 (# names2) c)
  (print (.. day " Part II: " p)))
