(local day "AOC 2015 Day 4")
(local input "bgvyzdsv")

(local md5 (require :md5))

(fn leading-zeros [st num]
  (var ret true)
  (for [i 1 num]
    (when (not (= (string.sub st i i) "0"))
      (set ret false)))
  ret)

;; Part I
(do
  (var num 0)
  (while (not (leading-zeros (md5.sumhexa (.. input num)) 5))
    (set num (+ num 1)))
  (print (.. day "  Part I:  " num)))

;; 254575

;; Part II
(do
  (var num 0)
  (while (not (leading-zeros (md5.sumhexa (.. input num)) 6))
    (set num (+ num 1)))
  (print (.. day "  Part II: " num)))

;; 1038736
