;; icollect
(icollect [_ v (ipairs [1 2 3 4 5])]
  (+ v 1))

(fn map [array fun]
  (icollect [_ v (ipairs array)] (fun v)))

(map [1 2 3 4] #(+ $1  1))

(fn filter [array fun]
  (icollect [_ v (ipairs array)] (when (fun v) v)))

  ;; Accumulate
(accumulate [sum 0 key value (ipairs [1 2 3 4 5])]
  (+ sum 1))

(fn fold [array init fun]
  (accumulate [acc init key value (ipairs array)]
    (fun acc value)))

(fn ifold [array init fun]
  (accumulate [acc init key value (ipairs array)]
    (fun acc key value)))

;; facuumalte
(faccumulate [n 0 i 1 5] (+ n i))

(fn split0 [st]
  (fcollect [i 1 (# st)]
    (string.sub st i i)))

(fn split [st key]
  (icollect [s (string.gmatch st (.. "[^" key "]+"))] s))


;; (icollect [a b c (string.gmatch "1,2,3\n4,5,6" "(%d),(%d),(%d)")] [a b c])

(fn scollect [st key]
  (let [accu []
        iterator (string.gmatch st key)]
    (var v nil)
    (while (do (set v [(iterator)]) (> (# v) 0))
      (if (=  1 (# v))
          (table.insert accu (. v 1))
          (table.insert accu v)))
    accu))

(local str "1
2
3")

(-> (split0 "()()))")
    (map #(match $ ")" -1 "(" +1))
    (fold 0 #(math.max 0 (+ $2 $1)))
    )

(-> (split0 "))(())((()(((")
    (map #(match $ ")" 1 "(" -1))
    (ifold {:sum 0 :first-ng 0}
           (fn [{: sum : first-ng} key value]
             {:sum (+ sum value)
              :first-ng (match [first-ng (+ sum value)]
                          [0 -1] key
                          _ first-ng)})))


(-> (split0 "))(())((()(((")
    (map #(match $ ")" 1 "(" -1))
    (fold 0 #($)})


;; https://en.wikipedia.org/wiki/Heap%27s_algorithm alternative?
;; procedure generate(k : integer, A : array of any):
;;     if k = 1 then
;;         output(A)
;;     else
;;         // Generate permutations with k-th unaltered
;;         // Initially k = length(A)
;;         generate(k - 1, A)

;;         // Generate permutations for k-th swapped with each k-1 initial
;;         for i := 0; i < k-1; i += 1 do
;;             // Swap choice dependent on parity of k (even or odd)
;;             if k is even then
;;                 swap(A[i], A[k-1]) // zero-indexed, the k-th is at k-1
;;             else
;;                 swap(A[0], A[k-1])
;;             end if
;;             generate(k - 1, A)
;;         end for
;;     end if


(fn heaps [k ar]
  (fn swap [ar a b]
    (local t (. ar a))
    (tset ar a b)
    (tset ar b t))
  (if (= k 1) ar
      (do (heaps (- k 1) ar)
          (for [i 1 k]
            (if (= 0 (% k 2))
                (swap ar i k)
                (swap ar 1 k))
            (heaps (- k 1) ar))
          ar)))


(fn factorial [n]
  (var ret 1)
  (for [i 1 n]
    (set ret (* ret i)))
  ret)

;; geven l and i what is the permuted order of array ar

(fn permutation-index [ar l f i]
  (local index (math.floor (* l (/ i f))))

  )

(fn heaps [ar len fun]
  (if (> len 1)
      (for [i 1 len]
        (heaps ar (- len 1) fun)
        (if (= (% len 2) 1)
            (let [first (. ar 1)
                  last (. ar len)]
              (tset ar 1 last)
              (tset ar len first))
            (let [index (. ar i)
                  last (. ar len)]
              (tset ar i last)
              (tset ar len index))))
      (fun ar)))


