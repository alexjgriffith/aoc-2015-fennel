(local advent-of-code (require :lib.advent-of-code))
(advent-of-code.download-input-to-source 14 2015)
(local input (advent-of-code.day-to-string 14))
(local day "AOC 2015 Day 14")
(local {: map : fold : scollect : heaps : pt->hash : hash->pt : split : unique
        : merge-t}
       (require :lib.utils))

(import-macros {: incr} :macros)

(fn clamp [x low up] (math.min (math.max x low) up))

(fn sort [t fun] (table.sort t fun) t)

(local parse-string "(%w+) can fly (%d+) km/s for (%d+) seconds, but then must rest for (%d+) seconds.")

(local data (-> (scollect input parse-string)
                (map (fn [[who speed time rest]]
                       {: who
                        :speed (tonumber speed)
                        :time (tonumber time)
                        :rest (tonumber rest)}))))

(local duration 2503)

(fn dist [duration data]
  (let [{: rest : speed : time : who} data
        distance (* time speed)
        t-total (+ time rest)
        steps (/ duration (+ time rest))
        rem (- (* steps t-total) (* (math.floor steps) t-total))
        remainder (clamp rem 0 time)]
    [(+ (* distance (math.floor steps)) (* remainder speed)) who]))

(print (.. day " Part I:  "
           (. (-> (map data (partial dist duration)) (sort (fn [a b] (> (. a 1) (. b 1))))) 1 1)))

(let [deer (-> (map data (partial dist 1)) (sort (fn [a b] (> (. a 1) (. b 1))))
               (map (fn [a] [(. a 2) (. a 1)])) pt->hash)]
  (each [key value (pairs deer)]
    (tset deer key 0))
  (for [i 1 duration]
    (local total (-> (map data (partial dist i)) (sort (fn [a b] (> (. a 1) (. b 1))))))

    (local max (. total 1 1))
    (local deer-step (-> total (map (fn [a] [(. a 2) (. a 1)])) pt->hash))
    (each [key value (pairs deer-step)]
      (when (= max value)
        (tset deer key (+ (. deer key) 1)))))
  (print (.. day " Part II: " (. (-> deer hash->pt (sort #(> (. $1 2) (. $2 2)))) 1 2))))
