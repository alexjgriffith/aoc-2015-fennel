(local advent-of-code (require :lib.advent-of-code))
(advent-of-code.download-input-to-source 9 2015)
(local input (advent-of-code.day-to-string 9))
(local day "AOC 2015 Day 9")

(local {: map : filter : fold : split0 : split : scollect} (require :lib.utils))

;; 14:07

;; Tristram to AlphaCentauri = 34

(local data (-> (scollect input "(%w+) to (%w+) = (%d+)") (map (fn [[from to distance]] {: from : to :distance (tonumber distance)}))))

;; 14:11

(fn in [ar key] (fold ar false #(if (= key $2) true $1)))

(fn last [ar] (. ar (# ar)))

(fn first [ar] (. ar 1))

(fn select [ar key value]
  (filter ar  #(= (. $ key) value)))

(fn select2 [ar key value key2 value2]
  (filter ar  #(and (= (. $ key) value)
                    (= (. $ key2) value2))))


(fn merge-t [ar1 ar2]
  (local ar3 [])
  (each [key value (ipairs (or ar1 []))]
    (table.insert ar3 value))
  (each [key value (ipairs (or ar2 []))]
    (table.insert ar3 value))
  ar3)


(fn pmap [array fun]
  (icollect [k v (pairs array)] (fun k v)))

(local stars ["Tristram"
              "AlphaCentauri"
              "Snowdin"
              "Tambi"
              "Faerun"
              "Norrath"
              "Straylight"
              "Arbre"])

(local distances {"Tristram" 0
                  "AlphaCentauri" 0
                  "Snowdin" 0
                  "Tambi" 0
                  "Faerun" 0
                  "Norrath" 0
                  "Straylight" 0
                  "Arbre" 0})

(fn step [distances]
  (local next-distance [])
  (each [previous distance (pairs distances)]
    (local path (split previous "->"))
    (local from (last path))
    (each [_ to (ipairs stars)]
      (local key (.. previous "->" to))
      (when (not (in path to))
        (tset next-distance key (+ distance
                                   (or (?. (select2 data :to to :from from) 1 :distance)
                                       (?. (select2 data :to from :from to) 1 :distance)
                                       0))))))

  next-distance)



(fn min [t]
  (local t2 (icollect [k v (pairs t)] {:distance v :path k}))
  (table.sort t2 (fn [a b] (< a.distance b.distance)))
  (.. (. t2 1  :distance)
      ;; " ;; " (. t2 1  :path)
      ))

(print (.. day "  Part I:  "
           (-> distances
               step
               step
               step
               step
               step
               step
               step
               min)))

;; 16:33

(fn max [t]
  (local t2 (icollect [k v (pairs t)] {:distance v :path k}))
  (table.sort t2 (fn [a b] (> a.distance b.distance)))
  (.. (. t2 1  :distance)
      ;; " ;; " (. t2 1  :path)
      ))

(print (.. day "  Part II: "
           (-> distances
               step
               step
               step
               step
               step
               step
               step
               max)))

;; 16:35
