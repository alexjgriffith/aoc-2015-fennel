(local advent-of-code (require :lib.advent-of-code))
(advent-of-code.download-input-to-source 12 2015)
(local input (advent-of-code.day-to-string 12))
(local day "AOC 2015 Day 12")
(local {: map : filter : fold : split0 : split : scollect : in} (require :lib.utils))
(import-macros {: incr} :macros)
;; 18:18

(print (.. day " Part I:  "
           (-> (icollect [n (string.gmatch input "([-%d]+)")] (tonumber n))
               (fold 0 #(+ $1 $2)))))

;; 18:21

(local json (require :json))
;; this solution works with lua-json

(fn search-numbers [child accum]
  (var red false)
  (local children [])
  (local taccum [])
  (local t (if child.__array :array :object))
  (each [key value (pairs child)]
    ;; (pp (type value))
    (match [(type key) (type value)]
      [:string :string] (when (= value :red) (set red true))
      [_ :number] (when (~= key :__array) (table.insert taccum value))
      [_ :table] (table.insert children value)))
  (when (not red)
    (each [_ value (ipairs taccum)]
      (table.insert accum value))
    (each [_ child (ipairs children)]
        (search-numbers child accum)))
  accum)

(print (.. day " Part II: "
           (-> (json.decode input) (search-numbers []) (fold 0 #(+ $1 $2)))))

;; 18:44
