(local advent-of-code (require :lib.advent-of-code))
(advent-of-code.download-input-to-source 7 2015)
(local input (advent-of-code.day-to-string 7))

(local {: split} (require :split))
(local day "AOC 2015 Day 7")

;; 16:40

;; 123 -> x
;; 456 -> y
;; x AND y -> d
;; x OR y -> e
;; x LSHIFT 2 -> f
;; y RSHIFT 2 -> g
;; NOT x -> h
;; NOT y -> i

(local operations (split input "\n"))

(fn set-number [x]
  (if (string.find x "%d")
      (tonumber x)
      x))

(local function {})

;; DFS with caching to avoid inf loops
(fn apply-operation [vals cache key {: t : v1 : v2}]
  (if (. cache key)
      (. cache key)
      (let [ret ((. function t) vals cache key v1 v2)]
        (tset cache key ret)
        ret)))

(fn limit-number-65535 [num]
  (if (< num 0)
      (+ 65535 num 1)
      num))

(fn operation-double [fun]
  (fn [vals cache key value1 value2]
    (limit-number-65535
      (fun
     (match (type value1)
       :number (limit-number-65535 value1)
       :string (apply-operation vals cache value1 (. vals value1))
       )
     (match (type value2)
       :number (limit-number-65535 value2)
       :string (apply-operation vals cache value2 (. vals value2)))))))

(fn operation-single [fun]
  (fn [vals cache key value ]
    (limit-number-65535
     (fun
      (match (type value)
        :number (limit-number-65535 value)
        :string (apply-operation vals cache value (. vals value))
         )))))

(fn function.assign  [...]
  ((operation-single (fn [x] x)) ...))

(fn function.and  [...]
  ((operation-double bit.band) ...))

(fn function.or  [...]
  ((operation-double bit.bor) ...))

(fn function.rshift  [...]
  ((operation-double bit.rshift) ...))

(fn function.lshift  [...]
  ((operation-double bit.lshift) ...))

(fn function.not  [...]
  ((operation-single bit.bnot) ...))

(local test
       "123 -> x
456 -> y
x AND y -> d
x OR y -> e
x LSHIFT 2 -> f
y RSHIFT 2 -> g
NOT x -> h
NOT y -> i")

;; Part I
(let [vals []]
  (each [_ value (ipairs operations)]
    (when (~= value "")
      (local tags (split value " "))
    (local target (. tags (# tags)))
    (var A1 (->> 1 (. tags) set-number))
    (var A2 (->> 2 (. tags) set-number))
    (var A3 (->> 3 (. tags) set-number))    
    ;; determine operation
    (var operation :ASSIGN)
    (if  (string.match value :AND) (set operation :AND)
         (string.match value :OR) (set operation :OR)
         (string.match value :RSHIFT) (set operation :RSHIFT)
         (string.match value :LSHIFT) (set operation :LSHIFT)
         (string.match value :NOT) (set operation :NOT))
    (match operation
      :ASSIGN (tset vals target {:t :assign :v1 A1 })
      :AND (tset vals target {:t :and :v1 A1  :v2 A3})
      :OR (tset vals target {:t :or :v1 A1 :v2 A3 })
      :RSHIFT (tset vals target {:t :rshift :v1 A1 :v2 A3 }) 
      :LSHIFT (tset vals target {:t :lshift :v1 A1  :v2 A3})
      :NOT (tset vals target {:t :not :v1 A2})))
    )
  (local key :a)
  (local cache [])
  (print (.. day "  Part I:  " (apply-operation vals cache key (. vals key)))))
;; 16076
;; 18:01

;; Part II
(let [vals []]
  (each [_ value (ipairs operations)]
    (when (~= value "")
      (local tags (split value " "))
    (local target (. tags (# tags)))
    (var A1 (->> 1 (. tags) set-number))
    (var A2 (->> 2 (. tags) set-number))
    (var A3 (->> 3 (. tags) set-number))    
    ;; determine operation
    (var operation :ASSIGN)
    (if  (string.match value :AND) (set operation :AND)
         (string.match value :OR) (set operation :OR)
         (string.match value :RSHIFT) (set operation :RSHIFT)
         (string.match value :LSHIFT) (set operation :LSHIFT)
         (string.match value :NOT) (set operation :NOT))
    (match operation
      :ASSIGN (tset vals target {:t :assign :v1 A1 })
      :AND (tset vals target {:t :and :v1 A1  :v2 A3})
      :OR (tset vals target {:t :or :v1 A1 :v2 A3 })
      :RSHIFT (tset vals target {:t :rshift :v1 A1 :v2 A3 }) 
      :LSHIFT (tset vals target {:t :lshift :v1 A1  :v2 A3})
      :NOT (tset vals target {:t :not :v1 A2})))
    )
  (tset vals :b {:t :assign :v1 16076})
  (local key :a)
  (local cache [])
  (print (.. day "  Part II: " (apply-operation vals cache key (. vals key)))))

;; 2797

;; 18:06
