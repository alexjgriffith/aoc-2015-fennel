(local advent-of-code (require :lib.advent-of-code))
(advent-of-code.download-input-to-source 3 2015)
(local input (advent-of-code.day-to-string 3))

(local {: split} (require :split))
(local day "AOC 2015 Day 3")

(fn split0 [s]
  (let [ret []]
    (for [i 1 (# s)]
      (table.insert ret (string.sub s i i)))
  ret))

(local test [">" 2
             "^>v<" 4
             "^v^v^v^v^v" 2])


(local char-array (-> input (split "")))

(fn char->position [char]
  (match char
    "^" [0 1]
    "<" [-1 0]
    "v" [0 -1]
    ">" [1 0]
    ))

(local char-position (icollect [key value (ipairs char-array)] (char->position value)))


(fn location->string [location]
  (.. "{:x " (. location 1) " y: " (. location 2) "}"))

(do
  (var location [0 0])
  (var count 1)
  (local locations-visited [])
  (tset locations-visited (location->string location) 1)
  (each [key [x y] (ipairs char-position)]
    (set location [(+ (. location 1) x) (+ (. location 2) y)])
    (local location-string (location->string location))
    (if (. locations-visited location-string)
        (tset locations-visited location-string (+ (. locations-visited location-string)1))
        (do
          (tset  locations-visited location-string  1)
          (set count (+ count 1)))
        )   
    )
  (print (.. day "  Part I:  " count)))

;; part II

(do
  (var location-1 [0 0])
  (var location-2 [0 0])
  (var location [0 0])
  (var count 1)
  (local locations-visited [])
  (tset locations-visited (location->string location) 1)
  (each [key [x y] (ipairs char-position)]
    (if (= (% key 2) 0)
        (do (set location-1 [(+ (. location-1 1) x) (+ (. location-1 2) y)])
            (set location location-1))
        (do (set location-2 [(+ (. location-2 1) x) (+ (. location-2 2) y)])
            (set location location-2)))
    (local location-string (location->string location))
    (if (. locations-visited location-string)
        (tset locations-visited location-string (+ (. locations-visited location-string)1))
        (do
          (tset  locations-visited location-string  1)
          (set count (+ count 1)))
        )   
    )
  (print (.. day "  Part II: " count)))
