(fn incr [x by] `(do (set ,x (+ ,x ,by)) ,x))

{: incr}

