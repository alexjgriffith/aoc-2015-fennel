(local advent-of-code (require :lib.advent-of-code))
(advent-of-code.download-input-to-source 13 2015)
(local input (advent-of-code.day-to-string 13))
(local day "AOC 2015 Day 13")
(local {: map : filter : fold : split0 : split : scollect : in} (require :lib.utils))
(import-macros {: incr} :macros)
;;19:06

;; there has to be a better way to do this, the memory usage is nuts
;; https://en.wikipedia.org/wiki/Heap%27s_algorithm alternative?
;; see day-13-alt.fnl

(local relations
       (->
        input
        (scollect "(%w+) would (%w+) (%d+) happiness units by sitting next to (%w+).")
        (map (fn [[who status amount nextto]]
               {: who
                :amount (if (= status :gain) (tonumber amount) (* -1 (tonumber amount)))
                : nextto}
               ))))

(fn merge-t [ar1 ar2]
  (local ar3 [])
  (each [key value (ipairs (or ar1 []))]
    (table.insert ar3 value))
  (each [key value (ipairs (or ar2 []))]
    (table.insert ar3 value))
  ar3)

(fn select2 [ar key value key2 value2]
  (filter ar  #(and (= (. $ key) value)
                    (= (. $ key2) value2))))

(fn unique [array]
  (local u {})
  (each [_ value (ipairs array)]
    (when (not (. u value))
      (tset u value value)))
  (icollect [_ v (pairs u)] v))

(fn last [ar] (. ar (# ar)))

(fn max [t]
  (local t2 (icollect [k v (pairs t)] {:distance v :path k}))
  (table.sort t2 (fn [a b] (> a.distance b.distance)))
  (values (. t2 1  :distance)
          (. t2 1  :path)))

(fn step [names relation-map haps]
  (fn in [ar key] (fold ar false #(if (= key $2) true $1)))
  (local next-haps [])
  (each [previous [hap path] (pairs haps)]
    (local from (last path))
    (each [_ to (ipairs names)]
      (local key (.. previous "->" to))
      (when (not (in path to))
        (tset next-haps key
              [(+ hap
                  (or (. relation-map (.. from "->" to)) 0)
                  (or (. relation-map (.. to "->" from)) 0))
               (merge-t path [to])]))))
  next-haps)

(fn link-first [names relation-map haps]
  (fn in [ar key] (fold ar false #(if (= key $2) true $1)))
  (local next-haps [])
  (each [previous [hap path] (pairs haps)]
    (local from (last path))
    (local to (. path 1))
    (local key (.. previous "->" to))
    (tset next-haps key (+ hap
                           (or (. relation-map (.. from "->" to)) 0)
                           (or (. relation-map (.. to "->" from)) 0))))
  next-haps)

;;["David" "Mallory" "George" "Frank" "Bob" "Carol" "Eric" "Alice"]
(local names (-> (merge-t (-> relations (map #(. $ :who)))
             (-> relations (map #(. $ :nextto))))
                 unique))

(local relation-map
       (let [relation-map []]
         (each [_ from (ipairs names)]
           (each [_ to (ipairs names)]
             (local key (.. from "->" to))
             (tset relation-map key (?. (select2 relations :nextto to :who from) 1 :amount)))
           )
       relation-map))

(local haps {"David" [0 ["David"]]
             "Mallory" [0 ["Mallory"]]
             "George" [0 ["George"]]
             "Frank" [0 ["Frank"]]
             "Bob" [0 ["Bob"]]
             "Carol" [0 ["Carol"]]
             "Eric" [0 ["Eric"]]
             "Alice" [0 ["Alice"]]})

(local step1 (partial step names relation-map))

(local link-first1 (partial link-first names relation-map))

(local (without without-order)
       (-> haps
           step1
           step1
           step1
           step1
           step1
           step1
           step1
           link-first1
           max
           ))

(print (.. day " Part II: " without))

;; 19:36

(local haps2 {"David" [0 ["David"]]
             "Mallory" [0 ["Mallory"]]
             "George" [0 ["George"]]
             "Frank" [0 ["Frank"]]
             "Bob" [0 ["Bob"]]
             "Carol" [0 ["Carol"]]
             "Eric" [0 ["Eric"]]
             "Alice" [0 ["Alice"]]
             "Me" [0 ["Me"]]})

(local names2 (merge-t names [:Me]))

(local relation-map2
       (let [rm2 []]
         (each [key value (pairs relation-map)]
           (tset rm2 key value))
         (each [_ name (pairs names)]
           (tset rm2 (.. "Me->" name) 0)
           (tset rm2 (..  name "->Me") 0))
         rm2))

(local step2 (partial step names2 relation-map2))

(local link-first2 (partial link-first names2 relation-map2))

(local (with with-order)
       (-> haps2
           step2
           step2
           step2
           step2
           step2
           step2
           step2
           step2
           link-first2
           max))

(print (.. day " Part II: " with))

;; 20:09
