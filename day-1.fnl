(local advent-of-code (require :lib.advent-of-code))
(advent-of-code.download-input-to-source 1 2015)
(local input (advent-of-code.day-to-string 1))

(local {: split} (require :split))
(local day "AOC 2015 Day 1")

;; Part I
(local tests ["(())" 0
              "()()" 0
              "(((" 3
              "(()(()(" 3
              "))((((("  3
              "())" -1
              "))(" -1
              ")))" -3
              ")())())"  -3])

(fn stair-state-1 [str-array]
  (var floor 0)
  (each [index value (ipairs str-array)]
    (match value
      "(" (set floor (+ floor 1))
      ")" (set floor (- floor 1))))
  floor)

(when false
  (print "Test")
  (for [i 1 (# tests) 2]
    (print (-> (. tests i) (split "") stair-state-1) (. tests (+ i 1)))))

(print (.. day "  Part I:  " (-> input (split "") (stair-state-1))))

;; Part II
(fn stair-state-2 [str-array]
  (var floor 0)
  (var first false)
  (each [index value (ipairs str-array)]
    (match value
      "(" (set floor (+ floor 1))
      ")" (set floor (- floor 1)))
    (when (and (= floor -1) (not first))
      (set first index)))
  first)

(-> "()())" (split "") (stair-state-2))

(print (.. day "  Part II: " (-> input (split "") (stair-state-2))))
