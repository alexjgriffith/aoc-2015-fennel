(local advent-of-code (require :lib.advent-of-code))
(local {: split} (require :split))
(advent-of-code.download-input-to-source 6 2015)
(local input (advent-of-code.day-to-string 6))
(local day "AOC 2015 Day 6")


;;16:08
(local instructions [])
(each [string x1 y1 x2 y2 (string.gmatch input "(%w+) (%d+),(%d+) through (%d+),(%d+)\n")]
  (table.insert instructions
   {:instruction string
       :x1 (tonumber x1)
       :y1 (tonumber y1)
       :x2 (tonumber x2)
       :y2 (tonumber y2)}))

;; Part I
(fn modify-light [lights-on x1 y1 x2 y2 fun]
  (for [i x1 x2]
    (when (not (. lights-on i))
      (tset lights-on i []))
    (for [j y1 y2]
      (tset lights-on i j (fun (. lights-on i j)))))
  lights-on)

(fn sum-light [lights-on]
  (var ret 0)
  (each [_key column (ipairs lights-on)]
    (each [_key value (ipairs column)]
      (set ret (+ value ret))))
  ret)

(fn process-instruction [lights-on {: instruction : x1 : x2 : y1 : y2}]
  (modify-light lights-on x1 y1 x2 y2 (match instruction
    :on (fn [_] 1)
    :off (fn [_] 0)
    :toggle (fn [x] (if (= x 1) 0 1)))))

(do
  (local lights-on (modify-light [] 1 1 1000 1000 (fn [_] 0)))
  (each [_ instruction (ipairs instructions)]
    (process-instruction lights-on instruction))
  (print (.. day "  Part I:  " (sum-light lights-on))))

;; 16:30

;; Part II
(fn process-instruction-2 [lights-on {: instruction : x1 : x2 : y1 : y2}]
  (modify-light lights-on x1 y1 x2 y2 (match instruction
    :on (fn [x] (+ x 1))
    :off (fn [x] (math.max 0 (- x 1)))
    :toggle (fn [x] (+ x 2))))
  )

(do
  (local lights-on (modify-light [] 1 1 1000 1000 (fn [_] 0)))
  (each [_ instruction (ipairs instructions)]
    (process-instruction-2 lights-on instruction))  
  (print (.. day "  Part II: " (sum-light lights-on))))
;; 16:34
