(local advent-of-code (require :lib.advent-of-code))
(advent-of-code.download-input-to-source 11 2015)
(local input (advent-of-code.day-to-string 11))
(local day "AOC 2015 Day 11")
(local {: map : filter : fold : split0 : split : scollect} (require :lib.utils))
(import-macros {: incr} :macros)
;; 17:19
(local sequence (-> (split0 input)))

(local _letter->number
       (-> "abcdefghijklmnopqrstuvwxyz"
           (split0)
           ((fn [x] (collect [k v (ipairs x)] (values v k))))))

(local _number->letter
       (-> "abcdefghijklmnopqrstuvwxyz"
           (split0)))

(fn letter->number [letter] (. _letter->number letter))

(fn number->letter [number] (. _number->letter number))

(fn increase-letter [letter]
  (local next-number (+ (letter->number letter) 1))
  (local carry (if (> next-number 26) 1 0))
  (local next-letter (number->letter (if (= 0 carry) next-number 1)))
  (values next-letter carry))

(fn reverse [ar]
  (local ret [])
  (local l (# ar))
  (for [i 0 (- l 1)]
    (tset ret (- l i) (. ar (+ i 1))))
  ret)

(fn increase-array [array]
  (var (carry next-letter) (values 1 "a"))
  (->
   (icollect [_ letter (ipairs (reverse array))]
     (if (= carry 1)
         (do (set (next-letter carry) (increase-letter letter))
             next-letter)
         letter))
   reverse))

;; Passwords must include one increasing straight of at least three letters,
;; like abc, bcd, cde, and so on, up to xyz. They cannot skip letters;
;; abd doesn't count.

(fn rule-1 [array]
  "array must include an increasing run"
  (local narray (map array #(letter->number $)))
  (var ret false)
  (var first (. narray 1))
  (var second (. narray 2))
  (for [i 3 (# array)]
    (local third (. narray i))
    (when (= (+ first 2) (+ second 1) (+ third 0)) (set ret true))
    (set first second)
    (set second third))
  ret)

;; Passwords may not contain the letters i, o, or l, as these letters can be
;; mistaken for other characters and are therefore confusing.

(fn rule-2 [array]
  "exclude i o l"
  (fn in [ar key] (fold ar false #(if (= key $2) true $1)))
  (var ret true)
  (each [_ letter (ipairs array)]
    (when (in ["i" "o" "l"] letter) (set ret false)))
  ret)

;; Passwords must contain at least two different, non-overlapping pairs of
;; letters, like aa, bb, or zz.

(fn rule-3 [array]
  "array must include an increasing run"
  (var count 0)
  (var (first second third) (values "" "" ""))
  (for [i 1 (# array)]
    (local fourth (. array i))
    (match [(= third fourth) (= second third) (= first second)]
      [false _ _] (incr count 0)
      [true false _] (incr count 1)
      [true _ true] (incr count 1)
      [true true false] (incr count 0))
    (set first second)
    (set second third)
    (set third fourth))
  (>= count 2))

(rule-3 ["h" "x" "b" "x" "x" "a" "b" "c"])

(fn next-password [sequence]
  (let [new-sequence (increase-array sequence)]
  (values new-sequence
          (and (rule-1 new-sequence)
               (rule-2 new-sequence)
               (rule-3 new-sequence)))))

(do
  (var (next-sequence good-password) (next-password sequence))
  (while (not good-password)
    (set (next-sequence good-password) (next-password next-sequence)))
  (print (.. day " Part I:  " (-> next-sequence (fold "" #(.. $1 $2))))))

;; 18:00

(do
  (var count 0)
  (var (next-sequence good-password) (next-password sequence))
  (while (< count 2)
    (set (next-sequence good-password) (next-password next-sequence))
    (when good-password (incr count 1)))
  (print (.. day " Part II: " (-> next-sequence (fold "" #(.. $1 $2))))))

;; 18:02
