;; 12:22
(local advent-of-code (require :lib.advent-of-code))
(advent-of-code.download-input-to-source 5 2015)
(local input (advent-of-code.day-to-string 5))

(local {: split} (require :split))
(local day "AOC 2015 Day 5")

;; ["aeiou" 3]
;; ["duplicate" 2]
;; ["excludes" "ab" "cd" "pq" "xy"]

(local lines (-> input (split "\n")))


(local test
       ["ugknbfddgicrmopn" true
        "aaa" true
        "jchzalrnumimnmhp" false
        "haegwjzuvuyypxyu" false
        "dvszwmarrgswjxmb" false])

(fn process-string [s]
  (local array (split s ""))
  (var previous "")
  (local prohibited {:ab true :cd true :pq true :xy true})
  (local vowel {:a true :e true :i true :o true :u true})
  (var vowel-count 0)
  (var duplicate-count 0)
  (var prohibited-count 0)
  (each [key letter (ipairs array)]
    (when (. vowel letter)
      (set vowel-count (+ vowel-count 1)))
    (when (= letter previous)
      (set duplicate-count (+ duplicate-count 1)))
    (when (. prohibited (.. previous letter))
      (set prohibited-count (+ prohibited-count 1)))
    (set previous letter)
    )
  (and (>= vowel-count 3 ) (>= duplicate-count 1) (= prohibited-count 0))
  )

(process-string "ugknbfddgicrmopn")
(process-string "jchzalrnumimnmhp")

(do
  (var count 0)
  (each [key value (ipairs lines)]
    (when (process-string value)
      (set count (+ count 1))))
  (print (.. day "  Part I:  " count)))

;; 12:32

(fn process-string-2 [s]
  (local array (split s ""))
  (var previous-1 "")
  (var previous-2 "")
  (var pair-count 0)
  (var between-count 0)
  (var previous-pairs {})
  (each [_key letter (ipairs array)]
    (when (= letter previous-2)
      (set between-count (+ between-count 1)))
    
    (local cs (.. previous-1 letter))
    (local ps (.. previous-2 previous-1))
    
    (local cp (or (. previous-pairs cs) 0))
    (local cpp (or (. previous-pairs ps) 0))
    
    (if (> cp 1)
      (set pair-count (+ pair-count 1))
      (and (= cp 1) (~= cs ps))
      (set pair-count (+ pair-count 1)))
    
    (when  (~= "" previous-1)
      (tset previous-pairs cs (+ cp 1)))
    (set previous-2 previous-1)     
    (set previous-1 letter)
    )
  (and (> pair-count 0) (> between-count 0))
  )

;; 434 too high

;; 40

;; 29

;; 41

;; 82

;; 71

(do
  (var count 0)
  (each [key value (ipairs lines)]
    (when (process-string-2 value)
      (set count (+ count 1))))
  (print (.. day "  Part II: " count)))

;; 69

(when false
  (local test-2 {:qjhvhtzxzqqjkmpb true
                 :xxyxx true
                 :uurcxstgmygtbstg false
                 :ieodomkazucvgmuy false})

  (each [key value (pairs test-2)]
    (print key (process-string-2 key) value)))
;; 13:35
