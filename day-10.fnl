(local advent-of-code (require :lib.advent-of-code))
(advent-of-code.download-input-to-source 10 2015)
(local input (advent-of-code.day-to-string 10))
(local day "AOC 2015 Day 10")
(local {: map : filter : fold : split0 : split : scollect} (require :lib.utils))
(import-macros {: incr} :macros)
;; 17:02

(local sequence (-> (split0 input) (map #(tonumber $))))

(fn rle [s]
  (local ret [])
  (var current (. s 1))
  (var count 0)
  (each [_ value (ipairs s)]
    (if (= value current)
        (set count (+ count 1))
        (do
          (table.insert ret count)
          (table.insert ret current)
          (set count 1)
          (set current value))))
  (table.insert ret count)
  (table.insert ret current)
  ret)

(do
  (var s sequence)
  (for [i 1 40]
    (set s (rle s)))
  (print (.. day " Part I:  " (# s))))

;; 17:12

(do
  (var s sequence)
  (for [i 1 50]
    (set s (rle s)))
  (print (.. day " Part II: " (# s))))

;; 17:12
